import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//My Dashboard component
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';


@NgModule({
  declarations: [
    AdminDashboardComponent
  ],
  imports: [
    BrowserModule
  ],

})

export class DashboardModule { }
