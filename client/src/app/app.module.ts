import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BlockUIModule } from 'ng-block-ui';




//My Modules
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';
import { FontEndModule } from './font-end/font-end.module';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { MenuManagementModule } from './menu-management/menu-management.module';

//App Component
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    BlockUIModule.forRoot(),
    
    AppRoutingModule,
    LayoutModule,
    FontEndModule,
    AuthModule,
    DashboardModule,
    MenuManagementModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
