import { Injectable, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root' /* by using providerIn we don't need to import service on provider array in module */
})
export class LocalStorageService {

    constructor(private zone: NgZone, private router: Router) {
    }

    public getCurrentUser() {
        return localStorage.getItem('User');
    }

    public getAccessToken(): string {
        return localStorage.getItem('Authorization');
    }

    public isAuthenticate(): boolean {
        if (localStorage.getItem('IsAuthenticate') == 'true')
            return true;
        else
            return false;
    }

    header = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": this.getAccessToken()
    });

    httpOptions = {
        headers: this.header
      };

    public getAuthenticationHeader(): any {
        return this.httpOptions;
    }

    public setCurrentUser(userDetail) {
        localStorage.removeItem('Authorization');
        localStorage.setItem('Authorization', 'Bearer ' + userDetail.accessToken);
        localStorage.setItem('User', userDetail.user);
        localStorage.setItem('Expiration', userDetail.expiration);
        localStorage.setItem('IsAuthenticate', userDetail.isAuthenticate);
        //localStorage.setItem('user', JSON.stringify(userDetail))
    }

    public removeLogin() {
        localStorage.removeItem('Authorization');
        localStorage.removeItem('User');
        localStorage.removeItem('Expiration');
        localStorage.removeItem('IsAuthenticate');
        this.router.navigate(['/login']);
    }

}
