export class ApiUrl {
    static serverMode = false;
    static localUrl = 'http://localhost:53053/api/';
    static prodUrl = 'http://104.236.63.203:6005/api/';
    static baseTitle = 'Healthcare System | ';
    // static baseUrl: string = ApiUrl.localUrl;
  
    /* this is for production or development url */
    static prodMode = false;
    static baseUrl: string = ApiUrl.prodMode === true ? ApiUrl.prodUrl : ApiUrl.localUrl;
    static MASTER_URI_PORT = ApiUrl.prodMode === true ? ':3000/api/' : ':4055/';  
    static MASTER_URI = ApiUrl.baseUrl + ApiUrl.MASTER_URI_PORT;
  }
  