
export class LoginModel {
  emailaddress: string;
  //userName: string;
  password: string;
}
export class PasswordResetModel {
  emailaddress: string;
}
export class RegistrationModel {
  fullName: string;
  emailAddress: string;
  password: string;
  patientManagement: boolean;
  pharmacyManagement: boolean;
  registrationDate: string;
}
export class EmailConfirmationModel {
  userId: string;
  token: string;
}
export class ChangePasswordModel {
  userName: string;
  currentPassword: string;
  newPassword: string;
}

export class ResetPassword {
  public emailAddress: string;
  public token: string;
  public newPassword: string;
  public confirmPassword: string;
}

export class RoleModel {
  id: string;
  name: string;
  status: string;
}