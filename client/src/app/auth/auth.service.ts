import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';// 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ApiUrl } from '../shared/service/api.service';
import { RoleModel, LoginModel, ResetPassword, RegistrationModel, EmailConfirmationModel, ChangePasswordModel } from './auth.model';
import { LocalStorageService } from '../shared/service/local.storage.service';

@Injectable({
    providedIn: 'root' /* by using providerIn we don't need to import service on provider array in module */
})

export class AuthService{
    token: string;

    constructor(private http: HttpClient, private localStorageService: LocalStorageService) { }

    headers = new Headers({
        "Content-Type": "application/json"
    });

    onLogin(loginDetail: LoginModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/login');
        return this.http.post(url, loginDetail);
    }

    resetPasswordRequest(passwordResetModel: ResetPassword): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/resetPasswordRequest');
        return this.http.post(url, passwordResetModel);
    }

    resetPasswordConfirmation(passwordResetModel: ResetPassword): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/resetPasswordConfirmation');
        return this.http.post(url, passwordResetModel);
    }

    onRegistration(registrationModel: RegistrationModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/registerUser');
        return this.http.post(url, registrationModel);
    }

    emailConfirmation(emailConfirmationModel: EmailConfirmationModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/emailConfirmation');
        return this.http.post(url, emailConfirmationModel);
    }

    changePassword(changePasswordModel: ChangePasswordModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'auth/changePassword');
        return this.http.post(url, changePasswordModel);
    }



    //-------------------------------Role Management--------------------------------------------------

    InsertRole(model: RoleModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Role/Insert');
        return this.http.post(url, model, this.localStorageService.getAuthenticationHeader());
    }
    
    UpdateRole(model: RoleModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Role/Update');
        return this.http.put(url, model, this.localStorageService.getAuthenticationHeader());
    }

    DeleteRole(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Role/Delete?Id='+ id);
        return this.http.delete(url, this.localStorageService.getAuthenticationHeader());
    }

    GetRoleInformation(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Role/GetInformation?Id=' + id);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    GetRoleList(pageSize: number, pageNo: number, searchString: string, filterByStatus: string, orderBy: string, order: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Role/GetList?PageSize=' + pageSize + '&PageNo='+ pageNo + 
        '&SearchString=' + searchString +'&FilterByStatus='+ filterByStatus +
        '&OrderBy=' + orderBy +'&Order='+ order);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    //----------------------------------End------------------------------------------------------


}