import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../auth.service';
import { RegistrationModel } from '../auth.model';
import { ApiUrl } from '../../shared/service/api.service';
import { UtilityService } from '../../shared/service/utility.service';
import { LocalStorageService } from '../../shared/service/local.storage.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;
    registration: RegistrationModel = new RegistrationModel();
    registrationForm: FormGroup;
    
    submitted = false;
    errorMessage = "";
    successMessage = "";

    constructor(private formBuilder: FormBuilder, private authService: AuthService,
        private router: Router, private location: Location,
        private localStorageService: LocalStorageService,
        private utilityService: UtilityService, private titleService: Title) {
        this.titleService.setTitle(ApiUrl.baseTitle + "Registration");
    }

    ngOnInit() {
        this.registrationForm = this.formBuilder.group({
            fullName: ['', [Validators.required, Validators.maxLength(100)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            patientManagement: new FormControl(false),
            pharmacyManagement: new FormControl(false),
        });
    }

    get registrationFromControlStatus() { return this.registrationForm.controls; }

    clear() {
        this.registrationForm.get('fullName').setValue('');
        this.registrationForm.get('email').setValue('');
        this.registrationForm.get('password').setValue('');
        this.registrationForm.get('patientManagement').setValue('');
        this.registrationForm.get('pharmacyManagement').setValue('');
        this.clearMessage();
    }

    clearMessage() {
        this.errorMessage = "";
        this.successMessage = "";
    }

    goBack() {
        this.location.back();
    }

    onRegistration() {
        try {

            this.submitted = true;
            if (this.registrationForm.invalid) {
                return;
            }
            else if (this.registrationForm.get('patientManagement').value == false
                && this.registrationForm.get('pharmacyManagement').value == false) {
                alert("Please select minimum one module.")
                return;
            }

            this.blockUI.start("Registration Processing");

            this.registration.fullName = this.registrationForm.get('fullName').value;
            this.registration.emailAddress = this.registrationForm.get('email').value;
            this.registration.password = this.registrationForm.get('password').value;
            this.registration.patientManagement = this.registrationForm.get('patientManagement').value;
            this.registration.pharmacyManagement = this.registrationForm.get('pharmacyManagement').value;
            this.registration.registrationDate = this.utilityService.getCurrentDate();

            this.authService.onRegistration(this.registration).subscribe(response => {
                this.clearMessage();
                var val = response;
                if (val.success == true) {
                    this.successMessage = val.message;
                    this.router.navigate(['/email-verification/' + this.registrationForm.get('email').value]);
                }else{
                    this.errorMessage = val.message;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

}