import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { LocalStorageService } from '../../shared/service/local.storage.service';

@Component({
    selector: 'back-end-header',
    templateUrl: './back-end-header.component.html',
    styleUrls: ['./back-end-header.component.css']
})

export class BackEndHeaderComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    constructor(private router: Router, private localStorageService: LocalStorageService) { }

    ngOnInit() {
        
    }

    logout() {        
        try {
            this.blockUI.start("Logout Processing.");
            this.localStorageService.removeLogin();
            this.blockUI.stop();
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }
}