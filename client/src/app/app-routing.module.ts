import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth/auth-guard.service'; 

import { FrontEndLayoutComponent } from './layout/front-end-layout/front-end-layout.component';
import { BackEndLayoutComponent } from './layout/back-end-layout/back-end-layout.component';
import { PageNotFoundComponent } from './font-end/page-not-found/page-not-found.component';

// website
import { HomeComponent } from './font-end/home/home.component';
import { ContactComponent } from './font-end/contact/contact.component';
import { PricingComponent } from './font-end/pricing/pricing.component';
import { AboutComponent } from './font-end/about/about.component';

//Auth
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoginComponent } from './auth/login/login.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { EmailConfirmationComponent } from './auth/email-confirmation/email-confirmation.component';
import { EmailVerificationComponent } from './auth/email-verification/email-verification.component';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { ResetPasswordRequestComponent } from './auth/reset-password-request/reset-password-request.component';
import { ResetPasswordConfirmationComponent } from './auth/reset-password-confirmation/reset-password-confirmation.component';
import { ForgetPasswordConfirmationComponent } from './auth/forget-password-confirmation/forget-password-confirmation.component';
import { RoleManagementComponent } from './auth/role-management/role-management.component';

//Dashboard
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { DoctorAdminDashboardComponent } from './dashboard/doctor-admin-dashboard/doctor-admin-dashboard.component';
import { DoctorUserDashboardComponent } from './dashboard/doctor-user-dashboard/doctor-user-dashboard.component';
import { PharmacyAdminDashboardComponent } from './dashboard/pharmacy-admin-dashboard/pharmacy-admin-dashboard.component';
import { PharmacyUserDashboardComponent } from './dashboard/pharmacy-user-dashboard/pharmacy-user-dashboard.component';
import { DoctorPharmacyAdminDashboardComponent } from './dashboard/doctor-pharmacy-admin-dashboard/doctor-pharmacy-admin-dashboard.component';
import { DoctorPharmacyUserDashboardComponent } from './dashboard/doctor-pharmacy-user-dashboard/doctor-pharmacy-user-dashboard.component';

//Menu Management
import { MenuItemComponent } from './menu-management/menu-item/menu-item.component';
import { MenuComponent } from './menu-management/menu/menu.component';

const routes: Routes = [

  {
    path: '',
    component: FrontEndLayoutComponent,
    children: [

      //Webiste
      { path: '', component: HomeComponent },
      { path: 'home', component: HomeComponent },
      { path: 'about', component: AboutComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'pricing', component: PricingComponent },

      //Auth
      { path: 'registration', component: RegistrationComponent },
      { path: 'login', component: LoginComponent },
      { path: 'forget-password', component: ForgetPasswordComponent },
      { path: 'email-confirmation/:userId/:token', component: EmailConfirmationComponent },
      { path: 'email-verification/:emailAddress', component: EmailVerificationComponent },
      { path: 'reset-password-request/:emailAddress/:token', component: ResetPasswordRequestComponent },
      { path: 'reset-password-confirmation', component: ResetPasswordConfirmationComponent },
      { path: 'forget-password-confirmation', component: ForgetPasswordConfirmationComponent }
      
      // Page not found
      //{ path: 'page-not-found', component: PageNotFoundComponent },
      //{ path: '**', redirectTo: '/page-not-found' },

    ]
  },

  {
    path: '',
    component: BackEndLayoutComponent,
    canActivate: [AuthGuard],
    children: [

      // Auth
      { path: 'change-password', component: ChangePasswordComponent },
      { path: 'role-management', component: RoleManagementComponent },
      
      //Dashboard
      { path: 'admin-dashboard', component: AdminDashboardComponent },
      { path: 'doctor-admin-dashboard', component: DoctorAdminDashboardComponent },
      { path: 'doctor-user-dashboard', component: DoctorUserDashboardComponent },
      { path: 'pharmacy-admin-dashboard', component: PharmacyAdminDashboardComponent },
      { path: 'pharmacy-user-dashboard', component: PharmacyUserDashboardComponent },
      { path: 'doctor-pharmacy-admin-dashboard', component: DoctorPharmacyAdminDashboardComponent },
      { path: 'doctor-pharmacy-user-dashboard', component: DoctorPharmacyUserDashboardComponent },

      //Menu Management
      { path: 'menu-item', component: MenuItemComponent },
      { path: 'menu', component: MenuComponent }
    ]
  },

  {
    path: '',
    component: FrontEndLayoutComponent,
    children: [
      // Page not found
      { path: 'page-not-found', component: PageNotFoundComponent },
      { path: '**', redirectTo: '/page-not-found' },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
