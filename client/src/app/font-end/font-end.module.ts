import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//My Webpage component
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { PricingComponent } from './pricing/pricing.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    HomeComponent,
    ContactComponent,
    PricingComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule
  ],

})

export class FontEndModule { }
