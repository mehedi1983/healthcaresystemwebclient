
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiUrl } from 'src/app/shared/service/api.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    constructor(private titleService: Title) {
        this.titleService.setTitle(ApiUrl.baseTitle + "Contact");
    }

    ngOnInit() {
    }

}