
import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { ApiUrl } from 'src/app/shared/service/api.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    constructor(private titleService: Title) {
        this.titleService.setTitle(ApiUrl.baseTitle+ "Home");
    }

    ngOnInit() {
    }

}