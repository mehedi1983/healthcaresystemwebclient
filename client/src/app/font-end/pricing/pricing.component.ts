
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ApiUrl } from 'src/app/shared/service/api.service';

@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.html',
    styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

    constructor(private titleService: Title) {
        this.titleService.setTitle(ApiUrl.baseTitle + "Pricing");
    }

    ngOnInit() {
    }

}