import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

//My Directive
import { NumberOnlyDirective } from '../shared/directive/numbers-only.directive';

//Auth Component
import { MenuItemComponent } from '../menu-management/menu-item/menu-item.component';
import { MenuComponent } from '../menu-management/menu/menu.component';

@NgModule({
  declarations: [
    MenuItemComponent, MenuComponent, NumberOnlyDirective
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    RouterModule,
    FormsModule, ReactiveFormsModule,
  ]
})
export class MenuManagementModule { }
