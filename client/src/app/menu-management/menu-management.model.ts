
export class MenuItemModel {
    id: string;
    name: string;
    url: string;
    description: string;
    isTopNode: string;
    order: string;
    status: string;
    createdDate: string;
    modifiedDate: string;
    createdBy: string;
    modifiedBy: string;
  }
 
  export class MenuModel {
    id: string;
    name: string;
    description: string;
    menuItemId: string;
    parentMenuId: string;
    status: string;
    order: number;
    createdDate: string;
    modifiedDate: string;
    createdBy: string;
    modifiedBy: string;
  }