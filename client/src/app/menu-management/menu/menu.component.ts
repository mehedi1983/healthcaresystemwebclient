import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { MatDialog } from '@angular/material/dialog';

import { ApiUrl } from '../../shared/service/api.service';
import { UtilityService } from '../../shared/service/utility.service';
import { LocalStorageService } from '../../shared/service/local.storage.service';
import { MenuManagementService } from '../menu-management.service';
import { MenuModel } from '../menu-management.model';
import { SearchModel } from '../../../app/shared/model/search.model';

declare var $: any;

@Component({
    selector: 'menu-management',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    entryModel: MenuModel = new MenuModel();
    entryForm: FormGroup;
    id: string;

    itemOne=[];
    itemTwo=[];
    insertOperation = true;
    submitted = false;
    insertUpdateMessage = "";
    errorMessage = "";
    successMessage = "";
    get entryFromControlStatus() { return this.entryForm.controls; }

    constructor(private formBuilder: FormBuilder, private service: MenuManagementService,
        private route: ActivatedRoute, private router: Router, private location: Location,
        private localStorageService: LocalStorageService,
        private utilityService: UtilityService, private titleService: Title) {
        this.titleService.setTitle(ApiUrl.baseTitle + "Menu Item");
    }

    ngOnInit() {
        try {
            this.entryForm = this.formBuilder.group({
                name: ['', [Validators.required, Validators.maxLength(100)]],
                description: ['', [Validators.maxLength(100)]],
                menuItemId: ['', [Validators.required, Validators.maxLength(100)]],
                parentMenuId: ['', [Validators.maxLength(100)]],
                order: ['', [Validators.required]],
                status: ['', [Validators.maxLength(15)]]
            });
            this.searchModel.status = "All";
            this.searchModel.searchString = "";
            this.GetList(this.pagingConfiguration.itemsPerPage, this.pagingConfiguration.currentPage,
                this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
        }
        catch (ex) {

        }
    }

    goBack() {
        this.location.back();
    }

    //------------------------------------Data Table Declaration---------------------------------

    pagingConfiguration = {
        currentPage: 1,
        itemsPerPage: 5,
        totalItems: 0
    };

    order = "Id";
    orderBy = "Asc";

    col1: string = "";
    col2: string = "";
    col3: string = "";
    col4: string = "";
    col5: string = "";
    col6: string = "";
    col7: string = "";

    col1_arrow: string = "";
    col2_arrow: string = "";
    col3_arrow: string = "";
    col4_arrow: string = "";
    col5_arrow: string = "";
    col6_arrow: string = "";
    col7_arrow: string = "";

    searchModel: SearchModel = new SearchModel();
    list = [];

    selectItemPerPage(ev) {
        try {
            this.pagingConfiguration.itemsPerPage = ev.target.value;
            this.GetList(this.pagingConfiguration.itemsPerPage, 1,
                this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
            this.pagingConfiguration.currentPage = 1;
        }
        catch (ex) {

        }
    }

    selectStatus(ev) {
        try {
            this.searchModel.status = ev.target.value;
            this.GetList(this.pagingConfiguration.itemsPerPage, 1,
                this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
            this.pagingConfiguration.currentPage = 1;
        }
        catch (ex) {

        }
    }

    searchByString(ev) {
        try {
            setTimeout(() => {
                this.searchModel.searchString = ev.target.value;
                this.GetList(this.pagingConfiguration.itemsPerPage, 1,
                    this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
                this.pagingConfiguration.currentPage = 1;
            }, 2000);
        }
        catch (ex) {

        }
    }

    GetList(pageSize: number, pageNo: number, searchString: string, filterByStatus: string,
        orderBy: string, order: string) {
        try {

            this.blockUI.start("Loading....");

            this.service.GetMenuList(pageSize, pageNo, searchString, filterByStatus, orderBy, order).subscribe(response => {
                var val = response;
                if (val.success == true) {
                    this.list = val.value;
                    this.pagingConfiguration.totalItems = val.totalRowsCount;
                } else {
                    this.list = [];
                    this.pagingConfiguration.totalItems = 0;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    pageChange(newPage: number) {
        try {
            this.GetList(this.pagingConfiguration.itemsPerPage, newPage, this.searchModel.searchString,
                this.searchModel.status, this.order, this.orderBy);
            this.pagingConfiguration.currentPage = newPage;
        }
        catch (ex) {

        }
    }

    sorting(index: number) {
        try {
            if (index == 1) {
                this.order = "Id";

                this.col1 = "kt-datatable__cell--sorted";
                this.col2 = "";
                this.col3 = "";
                this.col4 = "";
                this.col5 = "";
                this.col6 = "";
                this.col7 = "";

                this.col2_arrow = "";
                this.col3_arrow = "";
                this.col4_arrow = "";
                this.col5_arrow = "";
                this.col6_arrow = "";
                this.col7_arrow = "";
                if (this.col1_arrow == "") {
                    this.orderBy = "Asc";
                    this.col1_arrow = "la-arrow-up";
                }
                else if (this.col1_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col1_arrow = "la-arrow-up";
                }
                else if (this.col1_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col1_arrow = "la-arrow-down";
                }
            } else if (index == 2) {
                this.order = "Name";
                this.col1 = "";
                this.col2 = "kt-datatable__cell--sorted";
                this.col3 = "";
                this.col4 = "";
                this.col5 = "";
                this.col6 = "";
                this.col7 = "";

                this.col1_arrow = "";
                this.col3_arrow = "";
                this.col4_arrow = "";
                this.col5_arrow = "";
                this.col6_arrow = "";
                this.col7_arrow = "";

                if (this.col2_arrow == "") {
                    this.orderBy = "Asc";
                    this.col2_arrow = "la-arrow-up";

                }
                else if (this.col2_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col2_arrow = "la-arrow-up";
                }
                else if (this.col2_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col2_arrow = "la-arrow-down";
                }
            }
            else if (index == 3) {
                this.order = "Url";
                this.col1 = "";
                this.col2 = "";
                this.col3 = "kt-datatable__cell--sorted";
                this.col4 = "";
                this.col5 = "";
                this.col6 = "";
                this.col7 = "";

                this.col1_arrow = "";
                this.col2_arrow = "";
                this.col4_arrow = "";
                this.col5_arrow = "";
                this.col6_arrow = "";
                this.col7_arrow = "";

                if (this.col3_arrow == "") {
                    this.orderBy = "Asc";
                    this.col3_arrow = "la-arrow-up";
                }
                else if (this.col3_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col3_arrow = "la-arrow-up";
                }
                else if (this.col3_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col3_arrow = "la-arrow-down";
                }
            }
            else if (index == 4) {
                this.order = "Order";
                this.col1 = "";
                this.col2 = "";
                this.col3 = "";
                this.col4 = "kt-datatable__cell--sorted";
                this.col5 = "";
                this.col6 = "";
                this.col7 = "";

                this.col1_arrow = "";
                this.col2_arrow = "";
                this.col3_arrow = "";
                this.col5_arrow = "";
                this.col6_arrow = "";
                this.col7_arrow = "";

                if (this.col4_arrow == "") {
                    this.orderBy = "Asc";
                    this.col4_arrow = "la-arrow-up";
                }
                else if (this.col4_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col4_arrow = "la-arrow-up";
                }
                else if (this.col4_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col4_arrow = "la-arrow-down";
                }
            }
            else if (index == 5) {
                this.order = "Description";
                this.col1 = "";
                this.col2 = "";
                this.col3 = "";
                this.col4 = "";
                this.col5 = "kt-datatable__cell--sorted";
                this.col6 = "";
                this.col7 = "";

                this.col1_arrow = "";
                this.col2_arrow = "";
                this.col3_arrow = "";
                this.col4_arrow = "";
                this.col6_arrow = "";
                this.col7_arrow = "";

                if (this.col5_arrow == "") {
                    this.orderBy = "Asc";
                    this.col5_arrow = "la-arrow-up";
                }
                else if (this.col5_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col5_arrow = "la-arrow-up";
                }
                else if (this.col5_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col5_arrow = "la-arrow-down";
                }
            }
            else if (index == 6) {
                this.order = "Status";
                this.col1 = "";
                this.col2 = "";
                this.col3 = "";
                this.col4 = "";
                this.col5 = "";
                this.col6 = "kt-datatable__cell--sorted";
                this.col7 = "";

                this.col1_arrow = "";
                this.col2_arrow = "";
                this.col3_arrow = "";
                this.col4_arrow = "";
                this.col5_arrow = "";
                this.col7_arrow = "";

                if (this.col6_arrow == "") {
                    this.orderBy = "Asc";
                    this.col6_arrow = "la-arrow-up";
                }
                else if (this.col6_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col6_arrow = "la-arrow-up";
                }
                else if (this.col6_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col6_arrow = "la-arrow-down";
                }
            }
            else if (index == 7) {
                this.order = "Is Top Node";
                this.col1 = "";
                this.col2 = "";
                this.col3 = "";
                this.col4 = "";
                this.col5 = "";
                this.col6 = "";
                this.col7 = "kt-datatable__cell--sorted";

                this.col1_arrow = "";
                this.col2_arrow = "";
                this.col3_arrow = "";
                this.col4_arrow = "";
                this.col5_arrow = "";
                this.col6_arrow = "";

                if (this.col7_arrow == "") {
                    this.orderBy = "Asc";
                    this.col7_arrow = "la-arrow-up";
                }
                else if (this.col7_arrow == "la-arrow-down") {
                    this.orderBy = "Asc";
                    this.col7_arrow = "la-arrow-up";
                }
                else if (this.col7_arrow == "la-arrow-up") {
                    this.orderBy = "Des";
                    this.col7_arrow = "la-arrow-down";
                }
            }
            this.GetList(this.pagingConfiguration.itemsPerPage, this.pagingConfiguration.currentPage,
                this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
        }
        catch (ex) {

        }
    }

    //------------------------------------Data Table Declaration---------------------------------    

    //---------------------------------------------Modal-----------------------------------------

    GetMenuInit() {
        try {

            this.blockUI.start("Loading....");

            this.service.GetMenuInit().subscribe(response => {
                var val = response;
                if (val.success == true) {
                    this.itemOne = val.value.menuItems;
                    this.itemTwo = val.value.parents;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    openModalInInsertMode() {
        this.clear();
        this.insertUpdateMessage = "New";
        this.GetMenuInit();
        $("#myModal").modal('show');
    }

    openModalInUpdateMode(id: string) {
        this.clear();
        this.insertOperation = false;
        this.insertUpdateMessage = "Update";
        $("#myModal").modal('show');
        this.getInformation(id);
    }

    closeModal() {
        $('#myModal').modal('hide');
    }

    //-------------------------------------------End Modal-----------------------------------------

    //------------------------------------------Clear Message--------------------------------------

    clear() {
        try {
            this.id = "";
            this.entryForm.get('name').setValue('');
            this.entryForm.get('description').setValue('');
            this.entryForm.get('menuItemId').setValue('');
            this.entryForm.get('parentMenuId').setValue('');
            this.entryForm.get('order').setValue('');
            this.entryForm.get('status').setValue('Active');
            this.insertOperation = true;
            this.clearMessage();
        }
        catch (ex) {

        }
    }

    clearMessage() {
        try {
            this.insertUpdateMessage = "";
            this.errorMessage = "";
            this.successMessage = "";
        }
        catch (ex) {

        }
    }

    //----------------------------------------End Clear Message-----------------------------------

    //------------------------------------------CURD Operation------------------------------------

    getInformation(id: string) {
        try {

            this.blockUI.start("Loading....");
            this.id = id;
            this.service.GetMenuInformation(id).subscribe(response => {
                var val = response;
                if (val.success == true) {
                    this.itemOne = val.value.menuInitModel.menuItems;
                    this.itemTwo = val.value.menuInitModel.parents;

                    this.entryForm.get('name').setValue(val.value.name);
                    this.entryForm.get('description').setValue(val.value.description);
                    this.entryForm.get('menuItemId').setValue(val.value.menuItemId);
                    this.entryForm.get('parentMenuId').setValue(val.value.parentMenuId);
                    this.entryForm.get('status').setValue(val.value.status);
                } else {
                    this.list = [];
                    this.pagingConfiguration.totalItems = 0;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    onInsert() {
        try {

            this.submitted = true;
            if (this.entryForm.invalid) {
                return;
            }

            this.blockUI.start("Saving processing...");

            this.entryModel.name = this.entryForm.get('name').value;
            this.entryModel.description = this.entryForm.get('description').value;
            this.entryModel.menuItemId = this.entryForm.get('menuItemId').value;
            this.entryModel.parentMenuId = this.entryForm.get('parentMenuId').value;
            this.entryModel.status = this.entryForm.get('status').value;
            this.entryModel.createdDate = this.utilityService.getCurrentDate();
            this.entryModel.createdBy = this.localStorageService.getCurrentUser();

            this.service.InsertMenu(this.entryModel).subscribe(response => {
                this.clearMessage();
                var val = response;
                if (val.success == true) {
                    this.successMessage = val.message;
                    this.closeModal();
                    this.GetList(this.pagingConfiguration.itemsPerPage, this.pagingConfiguration.currentPage, this.searchModel.searchString,
                        this.searchModel.status, this.order, this.orderBy);
                } else {
                    this.errorMessage = val.message;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    onUpdate() {
        try {

            this.submitted = true;
            if (this.entryForm.invalid) {
                return;
            }

            this.blockUI.start("Update processing...");

            this.entryModel.id = this.id;
            this.entryModel.name = this.entryForm.get('name').value;
            this.entryModel.description = this.entryForm.get('description').value;
            this.entryModel.menuItemId = this.entryForm.get('menuItemId').value;
            this.entryModel.parentMenuId = this.entryForm.get('parentMenuId').value;
            this.entryModel.status = this.entryForm.get('status').value;
            this.entryModel.modifiedDate = this.utilityService.getCurrentDate();
            this.entryModel.modifiedBy = this.localStorageService.getCurrentUser();

            this.service.UpdateMenu(this.entryModel).subscribe(response => {
                this.clearMessage();
                var val = response;
                if (val.success == true) {
                    this.successMessage = val.message;
                    this.closeModal();
                    this.GetList(this.pagingConfiguration.itemsPerPage, this.pagingConfiguration.currentPage, this.searchModel.searchString, this.searchModel.status, this.order, this.orderBy);
                } else {
                    this.errorMessage = val.message;
                }
                this.blockUI.stop();
            }, error => {
                this.blockUI.stop();
            });
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    onDelete(id) {
        try {
            if (confirm("Are you sure to delete?")) {
                this.blockUI.start("Delete processing...");

                this.service.DeleteMenu(id).subscribe(response => {
                    var val = response;
                    if (val.success == true) {
                        this.successMessage = val.message;
                        this.GetList(this.pagingConfiguration.itemsPerPage, this.pagingConfiguration.currentPage, this.searchModel.searchString,
                            this.searchModel.status, this.order, this.orderBy);
                    } else {
                        this.errorMessage = val.message;
                    }
                    this.blockUI.stop();
                }, error => {
                    this.blockUI.stop();
                });
            }
        }
        catch (ex) {
            this.blockUI.stop();
        }
    }

    //----------------------------------------End CURD Operation----------------------------------    

}
