import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';// 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { ApiUrl } from '../shared/service/api.service';
import { MenuItemModel, MenuModel } from './menu-management.model';
import { LocalStorageService } from '../shared/service/local.storage.service';

@Injectable({
    providedIn: 'root' /* by using providerIn we don't need to import service on provider array in module */
})

export class MenuManagementService{

    constructor(private http: HttpClient, private localStorageService: LocalStorageService,) { }

    //-------------------------------Menu Item--------------------------------------------------

    Insert(model: MenuItemModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'MenuItem/Insert');
        return this.http.post(url, model, this.localStorageService.getAuthenticationHeader());
    }
    
    Update(model: MenuItemModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'MenuItem/Update');
        return this.http.put(url, model, this.localStorageService.getAuthenticationHeader());
    }

    Delete(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'MenuItem/Delete?Id='+ id);
        return this.http.delete(url, this.localStorageService.getAuthenticationHeader());
    }

    GetInformation(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'MenuItem/GetInformation?Id=' + id);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    GetList(pageSize: number, pageNo: number, searchString: string, filterByStatus: string, orderBy: string, order: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'MenuItem/GetList?PageSize=' + pageSize + '&PageNo='+ pageNo + 
        '&SearchString=' + searchString +'&FilterByStatus='+ filterByStatus +
        '&OrderBy=' + orderBy +'&Order='+ order);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    //----------------------------------End------------------------------------------------------


    //---------------------------------Menu------------------------------------------------------

    InsertMenu(model: MenuModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/Insert');
        return this.http.post(url, model, this.localStorageService.getAuthenticationHeader());
    }

    UpdateMenu(model: MenuModel): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/Update');
        return this.http.put(url, model, this.localStorageService.getAuthenticationHeader());
    }

    DeleteMenu(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/Delete?Id='+ id);
        return this.http.delete(url, this.localStorageService.getAuthenticationHeader());
    }

    GetMenuInit(): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/GetMenuInit');
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    GetMenuInformation(id: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/GetInformation?Id=' + id);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    GetMenuList(pageSize: number, pageNo: number, searchString: string, filterByStatus: string, orderBy: string, order: string): Observable<any> {
        const url = (ApiUrl.baseUrl + 'Menu/GetList?PageSize=' + pageSize + '&PageNo='+ pageNo + 
        '&SearchString=' + searchString +'&FilterByStatus='+ filterByStatus +
        '&OrderBy=' + orderBy +'&Order='+ order);
        return this.http.get(url, this.localStorageService.getAuthenticationHeader());
    }

    //----------------------------------End------------------------------------------------------
}